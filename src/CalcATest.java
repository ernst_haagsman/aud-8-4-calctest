public class CalcATest{
	@Test
	public void c_calcA_overflow(){
		int x = (int)Math.pow(2,30);
		int[] a = new int[] { 1, 4 };

		test("Overflow shouldn't occur", 1073741824, x, a);

	}

	@Test
	public void c_calcA_division(){
		int x = 0;
		int[] a = new int[] {6,0};

		test("Division shouldn't be an issue", 6,x,a);
	}

	public void test(String msg, int should, int x, int a[]){
		Calc c = new Calc();
		Assert.assertEquals(msg, should, c.calcA(x, a));
	}
}
