public class CalcVTest{
	@Test
	public void c_calcV_overflow(){
		int x = (int)Math.pow(2,30);
		int y = (int)Math.pow(2,30);

		test("Overflow should not occur", 46340, x, y);
	}

	@Test
	public void c_calcV_division(){
		test("Int division check", 2, 2, 3);
	}

	public void test(String msg, int should, int x, int y){
		Calc c = new Calc();
		Assert.assertEquals(msg, should, c.calcV(x, y));
	}
}
