public class Assert{
	public static void assertEquals(String message, int should, int is){
		if(should != is)
			throw new AssertionError("value is " + is + " but should be " + should + ": " + message);
	}
}

