public class Calc{
	public int calcV(int x, int y){
		int z = x+y;
		z /= 9;
		x = (int)Math.round(Math.sqrt(z));
		return 3 * x;
	}
	
	public int calcA(int x, int a[]){
		for(int i=0; i<a.length; ++i){
			if(i % 2 == 0){
				x += a[i];
			}else{
				x -= a[i];
			}
			if(x % 2 == 0){
				x /= 4;
			}else{
				x *= 4;
			}
		}
		return x;
	}
}
