import java.lang.*;
import java.lang.reflect.*;
import java.lang.annotation.*;

public class Runner{	
	public static void main(String args[]) throws Exception{
		if(args.length < 1){
			System.err.println("missing class argument");
			System.exit(-1);
		}
		String tcln = args[0];
		ClassLoader cl = null;
		Class c = null;
		try{
			cl = ClassLoader.getSystemClassLoader();
			c = cl.loadClass(tcln);
		}catch (Exception e){
			System.err.println("could not load class");
			e.printStackTrace();
			System.exit(-1);
		}
		int cnt = 0;
		int suc = 0;
		for(Method meth : c.getMethods()) {
			if(!meth.isAnnotationPresent(Test.class)){
				continue;
			}
			if(meth.getParameterTypes().length != 0){
				continue;
			}
			try{
				++cnt;
				meth.invoke(c.newInstance());
			}catch(Exception e){
				System.err.println("Test with method " + meth.getName() + " failed");
				e.printStackTrace();
				continue;
			}
			++suc;
			System.err.println("Test with method " + meth.getName() + " OK");
		}
		System.out.println(suc + "/" + cnt + " tests executed sucefully");
	}
}
